## 3 April 2023

Added kanji recognition class to gdtoasterutils, with a workaround to make recognition independent of stroke order.

## 2 April 2023 - Project start

Ported kanji canvas system from 漢字完

[Exposed](https://github.com/toasterofbread/godot/commit/1ff2c0cc51048fbe5d941543235052a08c90386f) svg rendering in engine (port from Goost which doesn't have a 4.0 version)

Created ds4godot (now in [gdtoasterutils](https://github.com/toasterofbread/gdtoasterutils)) with working single-finger and mostly working multi-finger touchpad support.
